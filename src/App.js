import React, {useState, useEffect} from 'react';
import axios from 'axios'
import styles from './components/Styles'
import './App.css';

function App() {
  const [allData, setAllData] = useState([])
  const [filteredData, setFilteredData] = useState(allData)

  const handleSearch = (event)  => {
    let value = event.target.value.toLowerCase()
    let result = [];
    console.log(value)

    result = allData.filter((data) => {
      return data.title.search(value) !== -1;
    })
    setFilteredData(result);
  }

  useEffect(() => {
    axios('https://jsonplaceholder.typicode.com/albums/1/photos')
      .then((response) => {
        console.log(response.data)
        setAllData(response.data);
        setFilteredData(response.data);
      })
      .catch(err => {
        console.log('Error getting fake data: ' + err)
      })
  }, [])

  // const Logic = () => {
  //   return (
  //     <>
  //       <div style={{padding:10}}>
  //         {filteredData.map((value, index) => {
  //           return (
  //             <div>
  //               <div style={styles} key={value.id}>
  //                 {value.title}
  //               </div>
  //             </div>
  //           )
  //         })}
  //       </div>
  //     </>
  //   )
  // }

  // const BasicForm = () => {
  //   return (
  //     <>
  //       <div style={{ margin: '0 auto', marginTop: '10%' }}>
  //         <label>Search:</label>
  //         <input type="text" onChange={(event) => handleSearch(event)} />
  //       </div>
  //     </>
  //   )
  // }
  
  return (
    <div className="App">
      <div style={{ margin: '0 auto', marginTop: '10%' }}>
        <label>Search:</label>
        <input type="text" onChange={(event) => handleSearch(event)} />
      </div>
      <br />
      <br />
      <div style={{padding:10}}>
        {filteredData.map((value, index) => {
          return (
            <div>
              <div style={styles} key={value.id}>
                {value.title}
              </div>
            </div>
          )
        })}
      </div>
    </div>
  );
}

export default App;
