// import React from 'react'

const styles = {
  display: 'inline',
  width: '30%',
  height: 50, // 50px
  float: 'left',
  padding: 5, // 5px
  border: '0.5px solid black',
  marginLeft: 20,
  marginBottom: 10,
  marginRight: 10
}

export default styles;